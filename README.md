## Documentação

### Iniciar projeto no localhost

```
php -S localhost:8000
```

### Instalar packages npm

```
npm install
```

### Sass compiler

```
npm run watch-css
```
