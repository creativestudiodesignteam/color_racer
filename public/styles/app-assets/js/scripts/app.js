var App = {
  logout: () => {
    let link = 'app/ajax/logout.php';

    $.ajax({
      url: link,
      type: 'POST',
      success: function () {
        window.location = './home';
      }
    });
  }
}