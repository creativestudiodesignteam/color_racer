<?php

include 'app/core/connection.class.php';
include 'app/core/session.class.php';
include 'app/core/title.class.php';
include 'app/core/url.class.php';
include 'app/core/notify.class.php';
include 'app/core/components.class.php';

$db         = new Connection();
$url        = new URL();
$session    = new Session();
$components = new Components();

$db = $db->connect();
$session->start();



if (explode("/", $url->link)[0] === 'sistema') {
  $components->headerADM();
} elseif (explode("/", $url->link)[0] !== 'home' and explode("/", $url->link)[0] !== 'sistema') {
  $components->headerStructure();
} else {
  $components->header();
}

foreach ($_GET as $key => $value) if (!is_array($value)) $_GET[$key] = $url->filter($value);
foreach ($_POST as $key => $value) if (!is_array($value)) $_POST[$key] = $url->filter($value);
//var_dump($url);
?>

<?php

$url->page();

?>

<?php
if (explode("/", $url->link)[0] === "sistema") {
  $components->footerADM();
} elseif (explode("/", $url->link)[0] !== 'home' and explode("/", $url->link)[0] !== 'sistema') {
  $components->footerStructure();
} else {
  $components->footer();
}
?>

<?php
if ($url->link === 'sobre') {
  echo '<style type="text/css">
          .container {
            height: calc(100vh - 0px);
          }
  
          body {
            background-image: url(public/images/pretty/friends-race-full.jpg);
            background-position: center center;
            background-repeat: no-repeat;
            background-size: cover;
            // height: calc(100vh - 0px);
            width: 100%;
            }
    </style>';
} elseif ($url->link === 'contato') {
  echo '<style type="text/css">
          .site-wrapper {
              background-image: url(public/images/bg-contato.jpg);
              background-position: center center;
              background-repeat: no-repeat;
              background-size: cover;
              height: calc(100vh - 0px);
          }
    </style>';
} else {
  echo '<style type="text/css">
      .site-wrapper {
          background-image: none;
      }
  </style>';
}
?>