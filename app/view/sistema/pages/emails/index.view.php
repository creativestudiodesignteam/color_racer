<?php
include 'app/model/emails.model.php';
include 'app/controller/emailsDAO.php';
$emails = new Emails();
$emailsDAO = new EmailsDAO($db);

$listEmails = $emailsDAO->listAll();


if (!empty($_GET['remove_id'])) {
  $emails->setId($_GET['remove_id']);
  $emailsDAO->emails = $emails;
  $emailsDAO->delete();
  $url->redirect('/pages/emails/index&msg=success');
}

?>

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body">
      <section id="configuration">
        <?php
        if (!empty($_GET['msg'])) {
          if ($_GET['msg'] == "success") {
            $components->notify($_GET['msg'], '<string>Sucesso!</string> Operação realizada com sucesso.');
          } else {
            $components->notify($_GET['msg'], '<string>Erro!</string> Por favor tente novamente.');
          }
        }
        ?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Gerenciar Email</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  <p class="card-text">Aqui você ira poder fazer todo o gerenciamento da página selecionada</p>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Assunto</th>
                          <th>Telefone</th>
                          <th>Cidade</th>
                          <th>Mensagem</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($listEmails as $list) { ?>

                          <tr>
                            <td><?= $list['id'] ?></td>
                            <td><?= $list['name'] ?></td>
                            <td><?= $list['email'] ?></td>
                            <td><?= $list['subject'] ?></td>
                            <td><?= $list['tellphone'] ?></td>
                            <td><?= $list['city'] ?></td>
                            <td><?= $list['mensage'] ?></td>
                          </tr>

                        <?php } ?>


                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Id</th>
                          <th>Nome</th>
                          <th>Email</th>
                          <th>Assunto</th>
                          <th>Telefone</th>
                          <th>Cidade</th>
                          <th>Mensagem</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>