<?php

include 'app/model/kits.model.php';
include 'app/controller/kitsDAO.php';
$kits = new Kits();
$kitsDAO = new KitsDAO($db);

include 'app/model/events.model.php';
include 'app/controller/eventsDAO.php';
$events = new Events();
$eventsDAO = new EventsDAO($db);

include 'app/model/pictures.model.php';
include 'app/controller/picturesDAO.php';
$pictures = new Pictures();
$picturesDAO = new PicturesDAO($db);
include 'app/model/events_kits.model.php';
include 'app/controller/events_kitsDAO.php';
$events_kits = new Events_kits();
$events_kitsDAO = new Events_kitsDAO($db);

$notify = array('', '');
$listKits = $kitsDAO->listAll();




if (isset($_POST['events'])) {
  $form = array(
    $_POST['events'],
    $_POST['kits']

  );
  $events->setCities($form[0]['cities']);
  $events->setDate($form[0]['date']);
  $events->setPlace($form[0]['place']);
  $events->setUrl($form[0]['url']);
  $eventsDAO->events = $events;
  $fk_events = $eventsDAO->insert();

  if (!empty($fk_events)) {
    $kitsList = [];
    $urlList = [];
    $kitsCheckeds = [$kitsList, $urlList];
    foreach ($_POST['kits'] as $kitsChecked) {
      array_push($kitsCheckeds[0], $kitsChecked);
    }
    foreach ($_POST['kits_url'] as $urlChecked) {
      array_push($kitsCheckeds[1], $urlChecked);
    }
    /* var_dump($kitsCheckeds); *//* 
              echo (sizeof($kitsCheckeds)); */
    for ($i = 0; $i <= (sizeof($kitsCheckeds) - 1); $i++) {
      $events_kits->setFk_events($fk_events);
      $events_kits->setFk_kits($kitsCheckeds[0][$i]);
      $events_kits->setUrl($kitsCheckeds[1][$i]);
      $events_kitsDAO->events_kits = $events_kits;
      $events_kitsDAO->insert();
    }
  }

  $url->redirect('pages/events/index&msg=success');
}

?>

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body">
      <div class="row match-height">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title" id="basic-layout-icons">Formulario para cadastro de Eventos</h4>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                  <li>
                    <a data-action="reload">
                      <i class="ft-rotate-cw"></i>
                    </a>
                  </li>
                  <li>
                    <a data-action="expand">
                      <i class="ft-maximize"></i>
                    </a>
                  </li>
                  <li>
                    <a data-action="close">
                      <i class="ft-x"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <form class="form" method="POST">
                  <div class="form-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="timesheetinput1">Cidade</label>
                          <div class="position-relative has-icon-left">
                            <input type="text" id="timesheetinput1" class="form-control" placeholder="Cidade | Estado" name="events[cities]">
                            <div class="form-control-position">
                              <i class="ft-user"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="timesheetinput2">Local</label>
                          <div class="position-relative has-icon-left">
                            <input type="text" id="place" class="form-control" placeholder="Local do evento" name="events[place]">
                            <div class="form-control-position">
                              <i class="ft-map"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="date">Data</label>
                          <div class="position-relative has-icon-left">
                            <input type="date" id="date" class="form-control" name="events[date]">
                            <div class="form-control-position">
                              <i class="ft-message-square"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="url">Url</label>
                          <div class="position-relative has-icon-left">
                            <input type="text" id="url" class="form-control" placeholder="Url da venda de ingressos" name="events[url]">
                            <div class="form-control-position">
                              <i class="la la-briefcase"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions center">
                    <h1>Selecione os kits do evento</h1>
                  </div>
                  <div class="row">

                    <?php foreach ($listKits as $list) {
                      $fotos = $picturesDAO->getById($list['fk_picture']);
                    ?>
                      <div class="col-xl-4 col-md-6">
                        <div class="card">
                          <img class="img-fluid" src="../<?= $fotos['path'] . $fotos['name'] ?>" alt="Card image cap">
                          <div class="card-body">
                            <h4 class="card-title"><?= $list['name'] ?></h4>
                            <h6 class="card-subtitle text-muted"><?= $list['description'] ?></h6>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="form-group">
                                <div class="position-relative has-icon-left">
                                  <input type="url" name="kits_url[]" class="form-control" placeholder="Url da venda de ingressos">
                                  <div class="form-control-position">
                                    <i class="ft-radio"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                            <span class="float-left"><?= $list['value'] ?></span>
                            <span class="float-right">
                              <div class="skin skin-flat">
                                <input type="checkbox" name="kits[]" value="<?= $list['id'] ?>" id="input-<?= $list['id'] ?>">
                                <label for="input-15">Selecionar</label>
                              </div>
                            </span>
                          </div>
                        </div>
                      </div>
                    <?php } ?>

                  </div>


                  <div class="form-actions right">
                    <button type="submit" class="btn btn-primary">
                      <i class="la la-check-square-o"></i> Salvar
                    </button>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>