<?php
include 'app/model/events.model.php';
include 'app/controller/eventsDAO.php';
$events = new Events();
$eventsDAO = new EventsDAO($db);

$listEvents = $eventsDAO->listAll();


if (!empty($_GET['remove_id'])) {
  $events->setId($_GET['remove_id']);
  $eventsDAO->events = $events;
  $eventsDAO->delete();
  $url->redirect('/pages/events/index&msg=success');
}

?>

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body">
      <section id="configuration">
        <?php
        if (!empty($_GET['msg'])) {
          if ($_GET['msg'] == "success") {
            $components->notify($_GET['msg'], '<string>Sucesso!</string> Operação realizada com sucesso.');
          } else {
            $components->notify($_GET['msg'], '<string>Erro!</string> Por favor tente novamente.');
          }
        }
        ?>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Gerenciar Cidades</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                  <ul class="list-inline mb-0">
                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                    <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                    <li><a data-action="close"><i class="ft-x"></i></a></li>
                  </ul>
                </div>
              </div>
              <div class="card-content collapse show">
                <div class="card-body card-dashboard">
                  <a class="btn btn-sm btn-primary box-shadow-2 round btn-min-width pull-right" href="pages/events/create">Cadastrar Cidades</a>
                  <p class="card-text">Aqui você ira poder fazer todo o gerenciamento da página selecionada</p>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered zero-configuration">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Nome</th>
                          <th>Local</th>
                          <th>Evento</th>
                          <th>Data</th>
                          <th>Ações</th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($listEvents as $list) { ?>

                          <tr>
                            <td><?= $list['id'] ?></td>
                            <td><?= $list['cities'] ?></td>
                            <td><?= $list['place'] ?></td>
                            <td><?= $list['url'] ?></td>
                            <td><?= $list['date'] ?></td>
                            <td>
                              <div class="form-actions">

                                <a href="./pages/events/edit&id=<?= $list['id'] ?>" class="btn btn-warning">
                                  <i class="la la-edit text-white"></i>
                                </a>
                                <a href="./pages/events/index&remove_id=<?= $list['id'] ?>" class="btn btn-danger">
                                  <i class="ft-x text-white"></i>
                                </a>

                              </div>
                            </td>
                          </tr>

                        <?php } ?>


                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Id</th>
                          <th>Nome</th>
                          <th>Local</th>
                          <th>Evento</th>
                          <th>Data</th>
                          <th>Ações</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>