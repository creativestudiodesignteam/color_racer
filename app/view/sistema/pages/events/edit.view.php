<?php
include 'app/model/kits.model.php';
include 'app/controller/kitsDAO.php';
$kits = new Kits();
$kitsDAO = new KitsDAO($db);

include 'app/model/events.model.php';
include 'app/controller/eventsDAO.php';
$events = new Events();
$eventsDAO = new EventsDAO($db);

include 'app/model/pictures.model.php';
include 'app/controller/picturesDAO.php';
$pictures = new Pictures();
$picturesDAO = new PicturesDAO($db);
include 'app/model/events_kits.model.php';
include 'app/controller/events_kitsDAO.php';
$events_kits = new Events_kits();
$events_kitsDAO = new Events_kitsDAO($db);


$notify = array('', '');

if (!empty($_POST['idRemove'])) {
  $events_kits->setId($_POST['idRemove']);
  $events_kitsDAO->events_kits = $events_kits;
  $events_kitsDAO->delete();
  /*  $url->redirect('/pages/events/index&msg=success'); */
}

if (!empty($_GET['id'])) {

  $info = $eventsDAO->getById($_GET['id']);
  $listKits = $events_kitsDAO->getByIdAll($info['id']);
  $listKitsAll = $kitsDAO->listAll($info['id']);
  //kitsSelect


  if (!empty($info)) {

    if (isset($_POST['kits_url'])) {
      $form = array(
        $_POST['kits_url'],
      );
      $events_kits->setFk_events($info['id']);
      $events_kits->setFk_kits($form[0]['id']);
      $events_kits->setUrl($form[0][0]);
      $events_kitsDAO->events_kits = $events_kits;
      $events_kitsDAO->insert();
      $url->redirect('pages/events/edit&id=' . $info['id']);
    }
    if (isset($_POST['kits_selected'])) {
      $form = array(
        $_POST['kits_selected'],
      );
      $events_kits->setId($form[0]['id']);
      $events_kits->setFk_events($info['id']);
      $events_kits->setFk_kits($form[0]['idkit']);
      $events_kits->setUrl($form[0]['url']);
      $events_kitsDAO->events_kits = $events_kits;
      $events_kitsDAO->update();
      $url->redirect('pages/events/edit&id=' . $info['id']);
    }

    if (isset($_POST['events'])) {
      $form = array(
        $_POST['events'],
      );


      $events->setId($info['id']);
      $events->setCities($form[0]['cities']);
      $events->setPlace($form[0]['place']);
      $events->setDate($form[0]['date']);
      $events->setUrl($form[0]['url']);
      $eventsDAO->events = $events;
      $eventsDAO->update();

      $url->redirect('pages/events/index&msg=success');
    }

?>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-body">
          <div class="row match-height">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title" id="basic-layout-icons">Formulario para cadastro de cidades</h4>
                  <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                  </a>
                  <div class="heading-elements">
                    <ul class="list-inline mb-0">
                      <li>
                        <a data-action="collapse">
                          <i class="ft-minus"></i>
                        </a>
                      </li>
                      <li>
                        <a data-action="reload">
                          <i class="ft-rotate-cw"></i>
                        </a>
                      </li>
                      <li>
                        <a data-action="expand">
                          <i class="ft-maximize"></i>
                        </a>
                      </li>
                      <li>
                        <a data-action="close">
                          <i class="ft-x"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <?php



                ?>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <form class="form" method="POST">
                      <div class="form-body">

                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="timesheetinput1">Cidade</label>
                              <div class="position-relative has-icon-left">
                                <input type="text" value="<?= !empty($info['cities']) ? $info['cities'] : "" ?>" id="timesheetinput1" class="form-control" placeholder="Cidade | Estado" name="events[cities]">
                                <div class="form-control-position">
                                  <i class="ft-user"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="timesheetinput2">Local</label>
                              <div class="position-relative has-icon-left">
                                <input type="text" value="<?= !empty($info['place']) ? $info['place'] : "" ?>" id="place" class="form-control" placeholder="Local do evento" name="events[place]">
                                <div class="form-control-position">
                                  <i class="ft-map"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="date">Data</label>
                              <div class="position-relative has-icon-left">
                                <input type="date" value="<?= !empty($info['date']) ? $info['date'] : "" ?>" id="date" class="form-control" name="events[date]">
                                <div class="form-control-position">
                                  <i class="ft-message-square"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="url">Url</label>
                              <div class="position-relative has-icon-left">
                                <input type="text" value="<?= !empty($info['url']) ? $info['url'] : "" ?>" id="url" class="form-control" placeholder="Url da venda de ingressos" name="events[url]">
                                <div class="form-control-position">
                                  <i class="la la-briefcase"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-actions right">
                          <button type="submit" class="btn btn-primary">
                            <i class="la la-check-square-o"></i> Salvar
                          </button>
                        </div>
                    </form>
                    <div class="form-actions center">
                      <h1>Kits Selecionados</h1>
                    </div>
                    <div class="row">

                      <?php foreach ($listKits as $list) {
                        $kitsList = $kitsDAO->getById($list['fk_kits']);
                        $fotos = $picturesDAO->getById($kitsList['fk_picture']);
                      ?>
                        <div class="col-xl-4 col-md-6">

                          <div class="card">
                            <img class="img-fluid" src="../<?= $fotos['path'] . $fotos['name'] ?>" alt="Card image cap">
                            <div class="card-body">
                              <h4 class="card-title"><?= $kitsList['name'] ?></h4>
                              <h6 class="card-subtitle text-muted"><?= $kitsList['description'] ?></h6>
                            </div>

                            <form method="POST">
                              <div class="row">
                                <input type="hidden" name="kits_selected[id]" value="<?= $list['id'] ?>" id="">
                                <input type="hidden" name="kits_selected[idkit]" value="<?= $list['fk_kits'] ?>" id="">
                                <div class="col-md-2">
                                  <button class="btn btn-warning">
                                    <i class="ft-edit text-white"></i>
                                  </button>
                                </div>
                                <div class="col-md-10">
                                  <div class="form-group">

                                    <div class="position-relative has-icon-left">

                                      <input type="url" name="kits_selected[url]" class="form-control" value="<?= $list['url'] ?>" placeholder="Url da venda de ingressos">
                                      <div class="form-control-position">
                                        <i class="ft-radio"></i>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>

                            </form>

                            <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                              <span class="float-left"><?= $kitsList['value'] ?></span>
                              <div class="float-right">

                                <form method="POST">
                                  <input type="hidden" name="idRemove" value="<?= $list['id'] ?>">
                                  <button class="btn btn-danger">
                                    <i class="ft-trash text-white"></i>
                                  </button>
                                </form>
                              </div>
                            </div>

                          </div>

                        </div>
                      <?php } ?>
                    </div>
                    <div class="form-actions center">
                      <h1>Adicionar Kits</h1>
                    </div>

                    <div class="row">
                      <?php foreach ($listKitsAll as $list) {
                        $fotos = $picturesDAO->getById($list['fk_picture']);
                      ?>

                        <div class="col-xl-4 col-md-6">
                          <form method="POST">
                            <div class="card">
                              <img class="img-fluid" src="../<?= $fotos['path'] . $fotos['name'] ?>" alt="Card image cap">
                              <div class="card-body">
                                <h4 class="card-title"><?= $list['name'] ?></h4>
                                <h6 class="card-subtitle text-muted"><?= $list['description'] ?></h6>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <div class="position-relative has-icon-left">
                                      <input type="url" name="kits_url[]" class="form-control" placeholder="Url da venda de ingressos">
                                      <div class="form-control-position">
                                        <i class="ft-radio"></i>
                                      </div>
                                    </div>
                                    <input type="hidden" name="kits_url[id]" class="form-control" value="<?= $list['id'] ?>">
                                  </div>
                                </div>
                              </div>
                              <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
                                <span class="float-left"><?= $list['value'] ?></span>
                                <span class="float-right">
                                </span>
                              </div>
                              <button type="submit" class="btn btn-success">
                                <i class="ft-plus text-white"></i>
                              </button>
                            </div>
                          </form>
                        </div>

                      <?php } ?>
                    </div>
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
    </div>
    </div>

<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>