<?php
include 'app/model/pictures.model.php';
include 'app/controller/picturesDAO.php';
$pictures = new Pictures();
$picturesDAO = new PicturesDAO($db);

include 'app/model/kits.model.php';
include 'app/controller/kitsDAO.php';

$kits = new Kits();
$kitsDAO = new KitsDAO($db);

$notify = array('', '');
if (!empty($_GET['id'])) {
  $info = $kitsDAO->getById($_GET['id']);
  $picturesList = $picturesDAO->getById($info['fk_picture']);

  if (!empty($info)) {

    if (isset($_POST['kits'])) {
      $form = array(
        $_POST['kits'],
      );
      if (!empty($_FILES['pictures']['tmp_name'])) {
        $img = !empty($_FILES['pictures']['tmp_name']) ? $_FILES['pictures'] : '';
        if (!empty($img)) {

          $ext = ltrim(substr($img['name'], strrpos($img['name'], '.')), '.');
          if (in_array($ext, array('png', 'jpg', 'jpeg'))) {
            $imgName = md5(time()) . '.' . $ext;
            $dir = 'public/images/kits/';
            move_uploaded_file($img['tmp_name'], $dir . $imgName);
            $pictures->setName($imgName);
            $pictures->setPath($dir);
            $picturesDAO->pictures = $pictures;
            $fk_picture = $picturesDAO->insert();
          }
          $kits->setId($info['id']);
          $kits->setName($form[0]['name']);
          $kits->setValue($form[0]['value']);
          $kits->setDescription($form[0]['description']);
          if (!empty($fk_picture)) {
            $kits->setFk_picture($fk_picture);
          } else {
            $kits->setFk_picture(null);
          }
          $kitsDAO->kits = $kits;
          $kitsDAO->update();
          $url->redirect('pages/kits/index&msg=success');
        }
      } else {
        $kits->setId($info['id']);
        $kits->setName($form[0]['name']);
        $kits->setValue($form[0]['value']);
        $kits->setDescription($form[0]['description']);
        $kits->setFk_picture($info['fk_picture']);
        $kitsDAO->kits = $kits;
        $kitsDAO->update();
        $url->redirect('pages/kits/index&msg=success');
      }
    }
  }
}




/* $img = !empty($_FILES['pictures']['tmp_name']) ? $_FILES['pictures'] : '';

if (!empty($img)) {
  $ext = ltrim(substr($img['name'], strrpos($img['name'], '.')), '.');
  if (in_array($ext, array('png', 'jpg', 'jpeg'))) {
    $imgName = md5(time()) . '.' . $ext;
    $dir = 'public/images/kits/';
    move_uploaded_file($img['tmp_name'], $dir . $imgName);
    $pictures->setName($imgName);
    $pictures->setPath($dir);
    $picturesDAO->pictures = $pictures;
    $fk_picture = $picturesDAO->insert();
  }
} */
/* if (isset($_POST['kits'])) {
  $form = array(
    $_POST['kits'],

  );


  $kits->setName($form[0]['name']);
  $kits->setValue($form[0]['value']);
  $kits->setDescription($form[0]['description']);
  if (!empty($fk_picture)) {
    $kits->setFk_picture($fk_picture);
  } else {
    $kits->setFk_picture(null);
  }

  $kitsDAO->kits = $kits;
  $kitsDAO->insert();

  $url->redirect('pages/kits/index&msg=success'); */
//}



?>

<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-body">
      <div class="row match-height">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title" id="basic-layout-icons">Formulario para cadastro de Kits</h4>
              <a class="heading-elements-toggle">
                <i class="la la-ellipsis-v font-medium-3"></i>
              </a>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li>
                    <a data-action="collapse">
                      <i class="ft-minus"></i>
                    </a>
                  </li>
                  <li>
                    <a data-action="reload">
                      <i class="ft-rotate-cw"></i>
                    </a>
                  </li>
                  <li>
                    <a data-action="expand">
                      <i class="ft-maximize"></i>
                    </a>
                  </li>
                  <li>
                    <a data-action="close">
                      <i class="ft-x"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="card-content collapse show">
              <div class="card-body">
                <form class="form" method="POST" enctype="multipart/form-data">
                  <div class="form-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name">Nome</label>
                          <div class="position-relative has-icon-left">
                            <input type="text" id="name" class="form-control" placeholder="Digite o nome do kit" value="<?= $info['name'] ?>" name="kits[name]">
                            <div class="form-control-position">
                              <i class="ft-user"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="value">Valor</label>
                          <div class="position-relative has-icon-left">
                            <input type="text" id="value" class="form-control" placeholder="Valor do kit" value="<?= $info['value'] ?>" name="kits[value]">
                            <div class="form-control-position">
                              <i class="ft-map"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                    ?>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="pictures">Fotos</label>
                          <div class="position-relative has-icon-left">
                            <input type="file" id="place" class="form-control" name="pictures">
                            <div class="form-control-position">
                              <i class="ft-image"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="date">Data</label>
                          <div class="position-relative has-icon-left">
                            <textarea class="form-control" name="kits[description]" id="description" cols="30" rows="5">
                            <?= $info['description'] ?>
                            </textarea>
                            <div class="form-control-position">
                              <i class="ft-message-circle"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-actions right">
                    <button type="submit" class="btn btn-primary">
                      <i class="la la-check-square-o"></i> Salvar
                    </button>
                  </div>
                </form>

              </div>
            </div>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>