<?php
require('app/core/login.class.php');
require('app/core/password.class.php');

$login = new Login();
$password = new Password();
if (isset($_SESSION['id'])) {
  $url->redirect('home');
}
if (!empty($_POST['email']) && !empty($_POST['password'])) {
  $login->email = $login->filter($_POST['email']);
  $login->password = $password->encrypt($login->filter($_POST['password']));
  $login->join();
}
?>


<link rel="stylesheet" type="text/css" href="../public/styles/app-assets/css/core/colors/palette-gradient.css">
<link rel="stylesheet" type="text/css" href="../public/styles/app-assets/css/pages/login-register.css">


<div class="app-content content">
  <div class="content-wrapper">
    <div class="content-wrapper-before"></div>
    <div class="content-header row">
    </div>
    <div class="content-body">
      <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
          <div class="col-lg-4 col-md-6 col-10 box-shadow-2 p-0">
            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
              <div class="card-header border-0">
                <div class="text-center mb-1">
                  <img src="../public/images/logo.png" alt="branding logo">
                </div>
                <div class="font-large-1  text-center">
                  Login Administrador
                </div>
              </div>
              <div class="card-content">

                <div class="card-body">
                  <form class="form-horizontal" method="POST">

                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="email" class="form-control round" name="email" placeholder="Seu Email" required>
                      <div class="form-control-position">
                        <i class="ft-user"></i>
                      </div>
                    </fieldset>

                    <fieldset class="form-group position-relative has-icon-left">
                      <input type="password" class="form-control round" name="password" placeholder="Digite sua senha" required>
                      <div class="form-control-position">
                        <i class="ft-lock"></i>
                      </div>
                    </fieldset>

                    <div class="form-group text-center">
                      <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Login</button>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>
  </div>
</div>
<script src="../public/styles/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>