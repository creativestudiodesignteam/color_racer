<!DOCTYPE html>
<html lang="pt-br">
<?php

$events = new Events();
$eventsDAO = new EventsDAO($db);

$listAll = $eventsDAO->listAll();
$listT = [];
foreach ($listAll as $list) {
    array_push($listT, $list);
}
?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="CreativeDevDesign" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>


<div id="content" class="site-content">
    <div class="content-right">
        <div class="horizontal-slider image-slider-wrapper relative">
            <div class="swiper-wrapper image-slider slider">
                <div class="swiper-slide text-slide">
                    <div class="carousel-item-text">
                        <h1 class="entry-title title-generic"><span class="colors-01">5</span> Km</h1>
                        <h1 class="entry-title title-generic"><span class="colors-02">10</span> Cidades</h1>
                        <h1 class="entry-title title-generic"><span class="colors-03">200k</span> de Pessoas</h1>
                        <div class="carousel-content description-home top-20 bottom-20">
                            Mais que uma corrida,<br>uma experiência colorida
                        </div>

                        <div class="learn-button-size" id="container">
                            <button href="#cidades" class="learn-more">
                                <span class="circle" aria-hidden="true">
                                    <span class="icon arrow"></span>
                                </span>
                                <span class="button-text">Ver cidades</span>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="lines-home"></div>

                <div class="swiper-slide content-image-group">
                    <img src="public/images/pretty/group-person-home.png" class="image-home-group" alt="" />
                </div>

                <div class="swiper-slide aux-margin-right about-mobile">
                    <div class="carousel-item-image">
                        <h3 class="about-title colors-02">Uma corrida mágica cheia de</h3>
                        <h2 class="title-generic text-about-d">Explosão de <span class="colors-03">cores</span>, efeitos</h2>
                        <h2 class="title-generic text-about-d">Especiais e muita <span class="colors-01">diversão</span>.</h2>
                        <div class="carousel-content description-home top-10 bottom-10 about-description">
                            <p class="color-gray-light"> A Color Race conecta pessoas e inspira a vida mais colorida através de uma experiência única e <br>
                                exclusiva, onde a energia das cores transpira alegria.
                            </p>
                        </div>
                        <div class="learn-button-size" id="container">
                            <button class="learn-more">
                                <span class="circle" aria-hidden="true">
                                    <span class="icon arrow"></span>
                                </span>
                                <span class="button-text">Saiba mais</span>
                            </button>
                        </div>
                    </div>
                    <div class="splash-color"></div>
                </div>

                <div class="swiper-slide relative aux-overflow-photos">
                    <div class="cointainer-home-middel">
                        <img src="public/images/image_01.jpg" class="image-home-group-slide" alt="">
                        <img src="public/images/image_02.jpg" class="image-home-group-slide" alt="">
                        <img src="public/images/image_03.jpg" class="image-home-group-slide" alt="">
                    </div>
                    <div class="lines-about"></div>

                    <div class="images-random">
                        <img class="photos-radom" id="photos-radom" />
                    </div>
                </div>

                <div class="swiper-slide">
                    <div class="center-text general-count">
                        <div class="flex-w flex-sa-m cd100 bor1 p-t-42 p-b-22 p-l-50 p-r-50 respon1 countdown-cities">
                            <div class="flex-col-c-m wsize2">
                                <span class="l1-txt2 p-b-4 days colors-01 text-time-d">35</span>
                                <span class="m2-txt2">Dias</span>
                            </div>

                            <div class="flex-col-c-m wsize2">
                                <span class="l1-txt2 p-b-4 hours colors-02 text-time-d">17</span>
                                <span class="m2-txt2">Horas</span>
                            </div>

                            <div class="flex-col-c-m wsize2">
                                <span class="l1-txt2 p-b-4 minutes colors-03 text-time-d">50</span>
                                <span class="m2-txt2">Min</span>
                            </div>

                            <div class="flex-col-c-m wsize2">
                                <span class="l1-txt2 p-b-4 seconds colors-02 text-time-d">39</span>
                                <span class="m2-txt2">Seg</span>
                            </div>
                        </div>

                        <div class="name-city">
                            <h2 class="text-about-city">Proxima corrida em:</h2>
                            <h2 class="text-about-city colors-03">Sorocaba</h2>
                        </div>
                    </div>

                    <div class="hands-splash-color"></div>
                    <div class="lines-count"></div>
                </div>

                <div class="swiper-slide" id="cidades">
                    <div class="carousel-item-image runners-mobile">
                        <h3 class="about-title colors-01">Prepare-se para a proxima</h3>
                        <h2 class="title-generic text-about-d"><span class="colors-02">Corridas</span> nas seguintes <span class="colors-03">cidades</span></h2>

                        <div class="general-box-one">
                            <?php
                            for ($i = 0; $i < (sizeof($listT) / 2); $i++) {
                                $dateFormat = strftime('%d de %B', strtotime($list['date']));
                            ?>

                                <div class="box-animate-cities">
                                    <div id="genesisexpo_infobox_5e62bc74ae6e0" class="genesisexpo_module_infobox type_tile layout_top infobox_alignment_left  hover-animation">
                                        <div class="infobox_wrapper">
                                            <h6 class="infobox_subtitle colors-02"><?= $dateFormat ?></h6>
                                            <h3 class="infobox_title"><?= $listT[$i]['cities'] ?> <?= $listT[$i]['place'] ?></h3>
                                            <a class="infobox_button button-read-more" href="cidades&id=<?= $listT[$i]['id'] ?>"></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                        <div class="general-box-two">
                            <?php
                            for ($i = $i; $i < (sizeof($listT)); $i++) {
                                $dateFormat = strftime('%d de %B', strtotime($list['date']));

                            ?>
                                <div class="box-animate-cities">
                                    <div id="genesisexpo_infobox_5e62bc74ae6e0" class="genesisexpo_module_infobox type_tile layout_top infobox_alignment_left  hover-animation">
                                        <div class="infobox_wrapper">
                                            <h6 class="infobox_subtitle colors-02"><?= $dateFormat ?></h6>
                                            <h3 class="infobox_title"><?= $listT[$i]['cities'] ?> <?= $listT[$i]['place'] ?></h3>
                                            <a class="infobox_button button-read-more" href="cidades&id=<?= $listT[$i]['id'] ?>"></a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="carousel-item-image">
                            <div class="news-general">
                                <h3 class="about-title colors-01">Newsletter</h3>
                                <h2 class="title-generic text-about-d">Cadastre seu <span class="colors-02">e-mail</span> e fique</h2>
                                <h2 class="title-generic text-about-d">por dentro de todas as <span class="colors-01">novidades!</span></h2>
                                <p class="description-news">
                                    Digite seu endereço de e-mail para receber informações sobre a Color Race e nossos eventos, ofertas especiais,<br>
                                    parceiros e produtos de parceiros. Você pode cancelar ainscrição a qualquer momento e seu endereço de e-mail<br>
                                    será usado somente de acordocom nossa política de privacidade.
                                </p>

                                <form class="newsletter-input-group">
                                    <input type="email" class="newsletter-input" placeholder="Digite seu email*" />
                                    <button type="submit" class="btn-rounden"><i class="fa fa-send"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="background-newsletter-woman"></div>
                    </div>

                    <!-- <div class="swiper-slide">
                    <div class="separator"></div>
                </div> -->
                </div>
            </div>
        </div>
    </div> <!-- End Page Content -->

    <script>
        /* Random images */
        var description = [
            "public/images/image_01.jpg",
            "public/images/image_02.jpg",
            "public/images/image_03.jpg"
        ];

        var size = description.length
        var x = Math.floor(size * Math.random())
        document.getElementById('photos-radom').src = description[x];
    </script>