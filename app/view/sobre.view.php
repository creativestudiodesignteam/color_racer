
<div class="container page-about">
    <div class="row">
        <div class="col-12 col-xl-8">
            <h3 class="about-title colors-02">
            Sobre a Color Race
            </h3>
            <h1 class="entry-title title-generic">Bem-vindo a <span class="colors-01">corrida</span>
            mais <span class="colors-03">colorida</span> do país</h1>
            <div class="carousel-content description-home top-10 bottom-10 about-description">
            <p class="color-gray-light"> Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única, 
                onde a energia das cores transpiram alegria.
            </p>
            <p class="color-gray-light">Os participantes largam de branco e, durante o percurso, acontece explosões de diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final, uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.</p>
            <h1 class="entry-title title-generic line-modified"><span class="colors-01">5</span> Km<br>
            <span class="colors-02">10</span> Cidades<br>
            <span class="colors-03">200k</span> de Pessoas</h1>
        </div> <!-- end col -->
    </div>
    <div class="col-4">
        <div class="btn-about">
            <!-- <img class="" src="public/images/pretty/friends-race.jpg" alt=""> -->
            <a class="button-play" href="#" data-videourl="https://youtu.be/l8S3yT8JCOI">
                <img class="" src="public/images/play.png" alt="play button">
            </a>
        </div>
    </div> <!-- .row -->
</div> <!-- container -->