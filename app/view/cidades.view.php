<?php
//Includes
include 'app/model/kits.model.php';
include 'app/controller/kitsDAO.php';
include 'app/model/pictures.model.php';
include 'app/controller/picturesDAO.php';
include 'app/model/events_kits.model.php';
include 'app/controller/events_kitsDAO.php';
setlocale(LC_ALL, NULL);
setlocale(LC_ALL, 'pt_BR');

$kits = new Kits();
$kitsDAO = new KitsDAO($db);
$events = new Events();
$eventsDAO = new EventsDAO($db);
$pictures = new Pictures();
$picturesDAO = new PicturesDAO($db);
$events_kits = new Events_kits();
$events_kitsDAO = new Events_kitsDAO($db);

if (!empty($_GET['id'])) {
  $info = $eventsDAO->getById($_GET['id']);
  if (!empty($info)) {
    $listKitsEvent = $events_kitsDAO->getByIdAll($info['id']);
    $dia = strftime('%d', strtotime($info['date']));
    $mes = strftime('%B', strtotime($info['date']));


?>

    <div class="container page-city">
      <div class="row justify-content-center">
        <div class="col-12">
          <div class="banner-top img-header-city banner-top-1">
            <div class="text-banner-top">
              <div class="text-banner-span row">
                <h4 class="text-banner-contact text-top-banner title-generic">
                  <span class="colors-01 text-uppercase"><?= !empty($dia) ? $dia : "" ?>
                    <br><?= !empty($mes) ? $mes : "" ?>
                  </span>
                </h4>
                <h4 class="text-banner-contact text-top-banner title-generic ml-2">
                  <?= !empty($info['cities']) ? $info['cities'] : "" ?>
                  <br>
                  <?= !empty($info['place']) ? $info['place'] : "" ?>
                </h4>
              </div>
              <div class="icon-insc-inline">
                <div class="learn-button-size" id="container">
                  <button class="learn-more">
                    <span class="circle" aria-hidden="true">
                      <span class="icon arrow"></span>
                    </span>
                    <a id="urlEvent" href="<?= !empty($info['url']) ? $info['url'] : "" ?>" target="_BLANK" class="button-text">Inscreva-se</a>
                  </button>
                </div>

              </div>
            </div>
          </div>
        </div>

        <!-- ofertas -->
        <div class="kits-cards">
          <ul class="cards">
            <?php foreach ($listKitsEvent as $list) {
              $listKits = $kitsDAO->getById($list['fk_kits']);
              $fotos = $picturesDAO->getById($listKits['fk_picture']);
            ?>
              <li class="cards__item">
                <div class="card">
                  <div class="card_image-kits">
                    <img src="<?= $fotos['path'] ?><?= $fotos['name'] ?>" alt="">
                  </div>
                  <div class="card__content">
                    <h1 class="card__text"> <?= $listKits['name'] ?></h1>
                    <p class="card__text">
                      <?= $listKits['description'] ?>
                    </p>
                    <button onclick="window.open('<?= $list['url'] ?>');" class="btn-city btn--block card__btn icon-heart"> Eu quero!</button>
                  </div>
                </div>
              </li>
            <?php } ?>
          </ul>
        </div>

        <!-- div camisetas mobile -->
        <div class="tshirt-responsive">
          <!-- <h3 class="about-title colors-03 title-tshirt">Veja aqui os</h3>
        <h4 class="text-banner-contact title-generic" style="margin-bottom: 20px;">detalhes da <span class="colors-02"> camiseta</span></h4> -->
          <img src="public/images/tshirt-responsive.png" class="" alt="" />
        </div>

        <!-- div camisetas color -->
        <div class="banner-top camisetas-color bg-img-camisas">
          <div style="width: 33%; margin-right: 10px;">
            <img src="public/images/color-race-tamanhos.png" class="" alt="" />
          </div>
          <div style="width: 67%">
            <h3 class="about-title colors-03 title-tshirt">Veja aqui os</h3>
            <h4 class="text-banner-contact title-generic" style="margin-bottom: 20px;">detalhes da <span class="colors-02"> camiseta</span></h4>
            <img src="public/images/tshirt-sizes.png" class="tshirt-sizes" alt="" />
          </div>
        </div> <!-- end div camisetas color -->

        <div class="justify-center">
          <h3 class="about-title colors-01">Tem alguma dúvida?</h3>
          <h2 class="title-generic text-about-d" style="padding-top: 0px;">Estamos aqui para te <span class="colors-01">ajudar</span><br>
            veja nossas <span class="colors-02">dúvidas</span> <span class="colors-03">frequentes</span>
          </h2>
        </div>

        <!-- faq -->
        <div class="faqs-container">
          <div class="faq active">
            <h3 class="faq-title active">
              O que é a Color Race?
            </h3>
            <p class="faq-text color-gray-light">
              Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
              onde a energia das cores transpiram alegria.</p>
            <p class="faq-text color-gray-light">
              Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final,
              uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
            </p>
            <button class="faq-toggle">
              <i class="fas fa-chevron-down"></i>
              <i class="fas fa-times"></i>
            </button>
          </div>

          <div class="faq">
            <h3 class="faq-title">
              É necessário ser atleta, corredor
              ou ter treinamento para participar do evento?
            </h3>
            <p class="faq-text color-gray-light">
              Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
              onde a energia das cores transpiram alegria.</p>
            <p class="faq-text color-gray-light">
              Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final,
              uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
            </p>
            <button class="faq-toggle">
              <i class="fas fa-chevron-down"></i>
              <i class="fas fa-times"></i>
            </button>
          </div>

          <div class="faq">
            <h3 class="faq-title">
              Crianças podem participar?
            </h3>
            <p class="faq-text color-gray-light">
              Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
              onde a energia das cores transpiram alegria.</p>
            <p class="faq-text color-gray-light">
              Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final,
              uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
            </p>
            <button class="faq-toggle">
              <i class="fas fa-chevron-down"></i>
              <i class="fas fa-times"></i>
            </button>
          </div>

          <div class="faq">
            <h3 class="faq-title">
              As cores desaparecem com a lavagem?
            </h3>
            <p class="faq-text color-gray-light">
              Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
              onde a energia das cores transpiram alegria.</p>
            <p class="faq-text color-gray-light">
              Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final,
              uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
            </p>
            <button class="faq-toggle">
              <i class="fas fa-chevron-down"></i>
              <i class="fas fa-times"></i>
            </button>
          </div>

          <div class="faq">
            <h3 class="faq-title">
              As cores constituem perigo a saúde?
            </h3>
            <p class="faq-text color-gray-light">
              Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
              onde a energia das cores transpiram alegria.</p>
            <p class="faq-text color-gray-light">
              Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final,
              uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
            </p>
            <button class="faq-toggle">
              <i class="fas fa-chevron-down"></i>
              <i class="fas fa-times"></i>
            </button>
          </div>

          <div class="faq">
            <h3 class="faq-title">
              Que roupa deverei usar?
            </h3>
            <p class="faq-text color-gray-light">
              Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
              onde a energia das cores transpiram alegria.</p>
            <p class="faq-text color-gray-light">
              Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final,
              uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
            </p>
            <button class="faq-toggle">
              <i class="fas fa-chevron-down"></i>
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div> <!-- end faq -->

      </div>

    </div>

<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>