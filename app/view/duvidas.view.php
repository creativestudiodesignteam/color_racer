
<div class="container page-city">
  <div class="row justify-content-between">
    <div class="col-12 col-md-6 col-xl-4">
      <div class="sec-duvidas">
        <div class="title-duvidas">
          <h3 class="about-title colors-01">Suporte</h3>
          <h2 class="title-generic text-about-d" style="padding-top: 0px;">Precisando de <span class="colors-01">ajuda?</span>
          </h2>
          <p class="color-gray-light">Tem alguma dúvida sobre a Color Race Brasil. Estamos aqui para te ajudar a
          solucionar todas as suas dúvidas
          sobre o evento.<p>
        </div>
      </div>
    </div>

      <!-- div box duvidas -->
      <div class="col-12 col-md-6 col-xl-8">
        <div class="row box-duvidas">

        <!-- box 1 -->
          <div class="col-xl-6">
            <div class="box-animate-duvidas">
                <div id="genesisexpo_infobox_5e62bc74ae6e0" class="genesisexpo_module_infobox type_tile layout_top infobox_alignment_left  hover-animation">
                    <div class="infobox_wrapper box-duvidas">
                        <!-- icon-tickets -->
                        <img src="public/images/icon-tickets.png" class="" alt="" />
                        <h3 class="infobox_title">Compras e Ingressos</h3>
                        <p class="color-gray-light">Comprou e ainda não recebeu o seu ingresso? Desistiu de ir a Color Race? 
                        Saiba como atuar em cada uma das suas dúvidas.</p>
                        <a class="infobox_button button-read-more" href="cidades"></a>
                    </div>
                </div>
            </div>
          </div>
        
           <!-- box 2 -->
          <div class="col-xl-6">
            <div class="box-animate-duvidas">
                <div id="genesisexpo_infobox_5e62bc74ae6e0" class="genesisexpo_module_infobox type_tile layout_top infobox_alignment_left  hover-animation">
                    <div class="infobox_wrapper box-duvidas">
                        <img src="public/images/icon-partner.png" class="" alt="" />
                        <h3 class="infobox_title">Patrocinador</h3>
                        <p class="color-gray-light">Já pensou ser parceiro em um dos principais eventos de corrida do país? 
                        Saiba o que sua marca pode ganhar ao associe a Color Race.
                        </p>
                        <a class="infobox_button button-read-more" href="cidades"></a>
                    </div>
                </div>
            </div>
          </div>

           <!-- box 3 -->
          <div class="col-xl-6">
            <div class="box-animate-duvidas">
                <div id="genesisexpo_infobox_5e62bc74ae6e0" class="genesisexpo_module_infobox type_tile layout_top infobox_alignment_left  hover-animation">
                    <div class="infobox_wrapper box-duvidas">
                        <img src="public/images/icon-volunteer.png" class="" alt="" />
                        <h3 class="infobox_title">Seja um Voluntário</h3>
                        <p class="color-gray-light">Quer ser um voluntário? Saiba como é fácil você fazer parte, ajudar e ainda divertir muito.</p>
                        <a class="infobox_button button-read-more" href="cidades"></a>
                    </div>
                </div>
            </div>
          </div>

           <!-- box 4 -->
          <div class="col-xl-6">
            <div class="box-animate-duvidas">
                <div id="genesisexpo_infobox_5e62bc74ae6e0" class="genesisexpo_module_infobox type_tile layout_top infobox_alignment_left  hover-animation">
                    <div class="infobox_wrapper box-duvidas">
                        <img src="public/images/icon-conversation.png" class="" alt="" />
                        <h3 class="infobox_title">Ainda tem Dúvidas?</h3>
                        <p class="color-gray-light">Tem alguma dúvida sobre a Color Race Brasil – 
                        Estamos aqui para te ajudar a solucionar todas as suas dúvidas sobre o evento.
                        </p>
                        <a class="infobox_button button-read-more" href="cidades"></a>
                    </div>
                </div>
            </div>
          </div>

      </div>
    </div>
      <!-- end div box duvidas -->
    <div class="col-12 m-top80">
      <h3 class="about-title colors-01">Tem alguma dúvida?</h3>
      <h2 class="title-generic text-about-d" style="padding-top: 0px;">Estamos aqui para te <span class="colors-01">ajudar</span><br>
      veja nossas <span class="colors-02">dúvidas</span> <span class="colors-03">frequentes</span>
      </h2>
    </div>

    <!-- faq -->
    <div class="faqs-container">
      <div class="faq active">
        <h3 class="faq-title">
        É necessário ser atleta, corredor
        ou ter treinamento para participar do evento?
        </h3>
        <p class="faq-text color-gray-light">
        Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
        onde a energia das cores transpiram alegria.</p>
        <p class="faq-text color-gray-light">
        Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final, 
        uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
        </p>
        <button class="faq-toggle">
          <i class="fas fa-chevron-down"></i>
          <i class="fas fa-times"></i>
        </button>
      </div>
	
      <div class="faq">
        <h3 class="faq-title">
          Crianças podem participar?
        </h3>
        <p class="faq-text color-gray-light">
        Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
        onde a energia das cores transpiram alegria.</p>
        <p class="faq-text color-gray-light">
        Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final, 
        uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
        </p>
        <button class="faq-toggle">
          <i class="fas fa-chevron-down"></i>
          <i class="fas fa-times"></i>
        </button>
      </div>
	
      <div class="faq">
        <h3 class="faq-title">
          As cores desaparecem com a lavagem?
        </h3>
        <p class="faq-text color-gray-light">
        Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
        onde a energia das cores transpiram alegria.</p>
        <p class="faq-text color-gray-light">
        Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final, 
        uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
        </p>
        <button class="faq-toggle">
          <i class="fas fa-chevron-down"></i>
          <i class="fas fa-times"></i>
        </button>
      </div>
	
      <div class="faq">
        <h3 class="faq-title">
          As cores constituem perigo a saúde?
        </h3>
        <p class="faq-text color-gray-light">
        Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
        onde a energia das cores transpiram alegria.</p>
        <p class="faq-text color-gray-light">
        Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final, 
        uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
        </p>
        <button class="faq-toggle">
          <i class="fas fa-chevron-down"></i>
          <i class="fas fa-times"></i>
        </button>
      </div>
	
      <div class="faq">
        <h3 class="faq-title">
          Que roupa deverei usar?
        </h3>
        <p class="faq-text color-gray-light">
        Sucesso em todo o mundo, a Color Race Brasil conecta pessoas e inspira suas vidas com uma experiência única,
        onde a energia das cores transpiram alegria.</p>
        <p class="faq-text color-gray-light">
        Os participantes largam de branco e, a cada km, acontece uma explosão diferente de cores sobre eles. São 5 km de pura emoção e entretenimento. Ao final, 
        uma grande festa estará a espera de todos na linha chegada para celebrar a vida com muita música e diversão.
        </p>
        <button class="faq-toggle">
          <i class="fas fa-chevron-down"></i>
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div> <!-- end faq -->

  </div>
  
</div>