<?php
include 'app/phpmailer/PHPMailer.php';
include 'app/phpmailer/SMTP.php';
include 'app/phpmailer/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

include 'app/model/emails.model.php';
include 'app/controller/emailsDAO.php';

$emails = new Emails();
$emailsDAO = new EmailsDAO($db);

if (isset($_POST['emails'])) {
    $form = array(
        $_POST['emails'],

    );
    var_dump($form[0]);


    $mail = new PHPMailer(true);

    try {
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'rabelojunior105@gmail.com';
        $mail->Password = 'juniorabelo';
        $mail->Port = 587;
        $mail->setFrom('rabelojunior105@gmail.com');
        $mail->addAddress('rabelojunior105@gmail.com');
        $mail->isHTML(true);
        $mail->Subject = 'Contato';
        $mail->Body = '

        </b>Nome:' . $form[0]['name'] . ' </b><br>
        </b>E-mail:' . $form[0]['email'] . ' </b><br>
        </b>Telefone:' . $form[0]['tellphone'] . ' </b><br>
        </b>Assunto:' . $form[0]['subject'] . ' </b><br>
        </b>Cidade:' . $form[0]['city'] . ' </b><br>
        </b>Mensagem:' . $form[0]['mensage'] . ' </b><br>
        
        
        ';
        if ($mail->send()) {
            $emails->setName($form[0]['name']);
            $emails->setEmail($form[0]['email']);
            $emails->setSubject($form[0]['subject']);
            $emails->setTellphone($form[0]['tellphone']);
            $emails->setCity($form[0]['city']);
            $emails->setMensage($form[0]['mensage']);
            $emails->setSubject($form[0]['subject']);

            $emailsDAO->emails = $emails;
            $emailsDAO->insert();
            $url->redirect('contato&msg=success');
        } else {
            $url->redirect('contato&msg=danger');
        }
    } catch (Exception $error) {
        $url->redirect('contato&msg=danger');
        //echo ("Erro ao enviar mensagem:{$mail->ErrorInfo}");
    }
}

$events = new Events();
$eventsDAO = new EventsDAO($db);

$listAll = $eventsDAO->listAll();


?>

<div class="container page-contact">
    <?php
    if (!empty($_GET['msg'])) {
        if ($_GET['msg'] == "success") {
            $components->notify($_GET['msg'], '<string>Sucesso!</string> Operação realizada com sucesso.');
        } else {
            $components->notify($_GET['msg'], '<string>Erro!</string> Por favor tente novamente.');
        }
    }
    ?>
    <div class="row">


        <div class="col-12 col-xl-5">
            <div class="text-about carousel-item-text">
                <h3 class="about-title colors-02">
                    Fale Conosco
                </h3>
                <h1 class="entry-title title-generic">Deixe-nos uma
                    <span class="colors-01"> mensagem</span></h1>
                <div class="carousel-content description-home top-10 bottom-10 about-description">
                    <p class="color-gray-light p-contact"> Tem alguma dúvida sobre a Color Race?<br>
                        Preencha o formulário abaixo e nós entraremos em contato com você o mais rápido possível. Geralmente isso leva dois dias úteis.
                    </p>
                    <p class="color-gray-light p-contact">Nota: Todos os campos abaixo devem ser preenchidos para enviar sua dúvida. Se você precisar de auxílio técnico para se inscrever no evento, sua solicitação deve ser enviada pelo menos 72 horas úteis antes do evento em questão.</p>
                </div>
            </div>
        </div>

        <!-- form -->
        <div class="form-contact col-12 col-xl-7">
            <form method="POST">
                <div class="row p-0">
                    <div class="col">
                        <div class="newsletter-input-group">
                            <input type="text" name="emails[name]" class="newsletter-input input-control" placeholder="Digite seu nome*" />
                        </div>
                    </div>

                    <div class="col">
                        <div class="newsletter-input-group">
                            <input type="text" name="emails[email]" class="newsletter-input input-control" placeholder="Digite seu e-mail*" />
                        </div>
                    </div>
                </div>
                <!-- outra row -->
                <div class="row p-0">
                    <div class="col">
                        <div class="newsletter-input-group">
                            <input type="text" name="emails[tellphone]" class="newsletter-input input-control" placeholder="Telefone" />
                        </div>
                    </div>

                    <div class="col">
                        <div class="newsletter-input-group">
                            <select name="emails[subject]" class="newsletter-input input-control">
                                <option selected disabled>Selecione o assunto</option>
                                <?php
                                foreach ($listAll as $list) { ?>
                                    <option value="<?= $list['cities'] ?>"><?= $list['cities'] ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="newsletter-input-group w-100">
                    <select name="emails[city]" class="newsletter-input input-control">
                        <option selected disabled>Selecione o assunto</option>
                        <?php
                        foreach ($listAll as $list) { ?>
                            <option value="<?= $list['cities'] ?>"><?= $list['cities'] ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="newsletter-input-group w-100">
                    <input type="textarea" name="emails[mensage]" class="newsletter-input input-control" style="height: 200px" placeholder="Mensagem" />
                </div>

                <button type="submit" class="btn-enviar-contato bold">Enviar mensagem</button>
            </form>
        </div>
    </div>
    <!-- end form -->
</div>
</div>
</div> <!-- End Content -->