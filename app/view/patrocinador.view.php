<div class="container page-partners">
    <div class="row">
        <div class="col-12 col-xl-7">
          <div class="text-about">
            <h3 class="about-title colors-02">
                  Entre nessa conosco
            </h3>
            <h1 class="entry-title title-generic">Seja um <span class="colors-01">patrocinador</span>
            oficial <span class="colors-02">Color Race</span></h1>
            <div class="description-home top-10 bottom-10 about-description">
              <p class="color-gray-light bold"> A Color Race Brasil quer fazer parcerias com
              patrocinadores locais e nacionais nas áreas de
              comidas e bebidas, moda e beleza, bens de consumo,
              entretenimento e muito mais!
              </p>
              <p class="color-gray-light">Como nosso patrocinador, você vai poder ter uma experiência de positivação com pessoas que se encaixam nos seguintes perfis:
              </p>
                <ul class="color-gray-light">
                  <li>Homens e mulheres de 18 a 55 anos;</li>
                  <li>De classe média a alta;</li>
                  <li>Famílias com crianças de 5 a 18 anos;</li>
                  <li>Corredores e atletas;</li>
                  <li>Frequentadores de festivais;</li>
                  <li>Fãs extremamente engajados;</li>
                </ul>
            </div>
              <p class="color-gray-light">As atividades de patrocínio incluem anúncios em rádio, ações promocionais, propaganda visual, atividades no evento, naming rights, 
                distribuição e/ou venda de produtos. 
                Trabalhe conosco e desenvolva o plano de patrocínio que vai te ajudar a se comunicar e positivar a marca da melhor forma possível.</p>
              <h3 class="about-title colors-02 m-40">
                Oportunidade
              </h3>
              <h1 class="entry-title title-generic">De <span class="colors-03">Patrocínio</span>
              e <span class="colors-01">Merchandising</span></h1>
              <div class="">
                <p class="p-partners">Entre em contato a Leg Sports por telefone ou email</p>
                <p class="p-partners">Tel: +55 (31) 3504-1140</p>
                <p class="p-partners">Email: comercial@colorracebrasil.com.br</p>
                <img src="public/images/leg-sports.png" class="m-40 m-40-bottom" alt="Leg Sports">
              </div>
            </div>
        </div>
        <!-- pictures side right -->
        <div class="col-12 col-xl-5 pictures-partners d-none d-xl-block">
          <div class="container-home-middle">
              <img src="public/images/image_partners_1.jpg" class="image-home-group-slide" alt="">
              <img src="public/images/image_partners_2.jpg" class="image-home-group-slide" alt="">
              <img src="public/images/image_02.jpg" class="image-home-group-slide" alt="">
              <img src="public/images/image_03.jpg" class="image-home-group-slide" alt="">
              <img src="public/images/image_01.jpg" class="image-home-group-slide" alt="">
          </div>
        </div>

    </div> <!-- .row -->
</div> <!-- End Content -->







