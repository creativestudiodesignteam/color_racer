<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<?php
global $url;
date_default_timezone_set('America/Sao_Paulo'); // Hora oficial do Brasil.
?>
<?php if (isset($_SESSION['id'])) { ?>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">

    <base href="http://<?= $_SERVER['SERVER_NAME'] ?>/color_racer/sistema/configs">
    <link rel="stylesheet" type="text/css" href="public/styles/app-assets/css/pages/email-application.css">
    <title><?= $url->title(); ?></title>
    <link rel="apple-touch-icon" href="../public/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" href="../public/styles/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" href="../public/styles/app-assets/vendors/css/charts/chartist.css">
    <link rel="stylesheet" href="../public/styles/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">

    <link rel="stylesheet" type="text/css" href="../public/styles/app-assets/vendors/css/file-uploaders/dropzone.min.css">
    <link rel="stylesheet" type="text/css" href="../public/styles/app-assets/css/plugins/file-uploaders/dropzone.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" href="../public/styles/app-assets/css/bootstrap.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/colors.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../public/styles/app-assets/css/plugins/forms/checkboxes-radios.css">
    <link rel="stylesheet" type="text/css" href="../public/styles/app-assets/vendors/css/ui/prism.min.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" href="../public/styles/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/pages/chat-application.css">
    <link rel="stylesheet" href="../public/styles/app-assets/css/pages/dashboard-analytics.css">

    <link rel="stylesheet" type="text/css" href="../public/styles/app-assets/css/plugins/file-uploaders/dropzone.css">
    <!-- END: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../public/styles/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" href="../public/styles/assets/css/style.css">
    <script src="../public/styles/app-assets/js/scripts/app.js" type="text/javascript"></script>
    <!-- END: Custom CSS-->

  </head>


  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="app-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
          <li class="nav-item mr-auto"><a class="navbar-brand" href="home">
              <h3 class="brand-text">Dashboard</h3>
            </a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="navigation-background"></div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" nav-item"><a href="home"><i class="ft-home"></i><span class="menu-title" data-i18n="">Dashboard</span></a>
          </li>
          <li class=" nav-item"><a href="pages/kits/index"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Kits</span></a>
          </li>
          <li class=" nav-item"><a href="pages/events/index"><i class="ft-bookmark"></i><span class="menu-title" data-i18n="">Eventos</span></a>
          </li>
          <li class=" nav-item"><a href="pages/events/index"><i class="ft-mail"></i><span class="menu-title" data-i18n="">Email</span></a>
          </li>


        </ul>
      </div>
    </div>

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
              <li class="nav-item dropdown navbar-search"><a class="nav-link dropdown-toggle hide" data-toggle="dropdown" href="#"><i class="ficon ft-search"></i></a>
                <ul class="dropdown-menu">
                  <li class="arrow_box">
                    <form>
                      <div class="input-group search-box">
                        <div class="position-relative has-icon-right full-width">
                          <input class="form-control" id="search" type="text" placeholder="Search here...">
                          <div class="form-control-position navbar-search-close"><i class="ft-x"></i></div>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav float-right">
              <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown">
                  <i class="ficon ft-bell bell-shake" id="notification-navbar-link"></i>
                  <span class="badge badge-pill badge-sm badge-danger badge-up badge-glow">10</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                  <div class="arrow_box_right">
                    <li class="dropdown-menu-header">
                      <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6>
                    </li>
                    <li class="scrollable-container media-list w-100"><a href="javascript:void(0)">
                        <div class="media">
                          <div class="media-left align-self-center"><i class="ft-share info font-medium-4 mt-2"></i></div>
                          <div class="media-body">
                            <h6 class="media-heading info">New Order Received</h6>
                            <p class="notification-text font-small-3 text-muted text-bold-600">Lorem ipsum dolor sit amet!</p><small>
                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">3:30 PM</time></small>
                          </div>
                        </div>
                      </a><a href="javascript:void(0)">
                        <div class="media">
                          <div class="media-left align-self-center"><i class="ft-save font-medium-4 mt-2 warning"></i></div>
                          <div class="media-body">
                            <h6 class="media-heading warning">New User Registered</h6>
                            <p class="notification-text font-small-3 text-muted text-bold-600">Aliquam tincidunt mauris eu risus.</p><small>
                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">10:05 AM</time></small>
                          </div>
                        </div>
                      </a><a href="javascript:void(0)">
                        <div class="media">
                          <div class="media-left align-self-center"><i class="ft-repeat font-medium-4 mt-2 danger"></i></div>
                          <div class="media-body">
                            <h6 class="media-heading danger">New Purchase</h6>
                            <p class="notification-text font-small-3 text-muted text-bold-600">Lorem ipsum dolor sit ametest?</p><small>
                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Yesterday</time></small>
                          </div>
                        </div>
                      </a><a href="javascript:void(0)">
                        <div class="media">
                          <div class="media-left align-self-center"><i class="ft-shopping-cart font-medium-4 mt-2 primary"></i></div>
                          <div class="media-body">
                            <h6 class="media-heading primary">New Item In Your Cart</h6><small>
                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last week</time></small>
                          </div>
                        </div>
                      </a><a href="javascript:void(0)">
                        <div class="media">
                          <div class="media-left align-self-center"><i class="ft-heart font-medium-4 mt-2 info"></i></div>
                          <div class="media-body">
                            <h6 class="media-heading info">New Sale</h6><small>
                              <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Last month</time></small>
                          </div>
                        </div>
                      </a></li>
                    <li class="dropdown-menu-footer"><a class="dropdown-item info text-right pr-1" href="javascript:void(0)">Read all</a></li>
                  </div>
                </ul>
              </li>
              <li class="dropdown dropdown-user nav-item">
                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <span class="avatar avatar-online">
                    <span class="user-name text-bold-700 ml-1"><?= $_SESSION['name'] ?></span>
                  </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="avatar avatar-online"><span class="user-name text-bold-700 ml-1"><?= $_SESSION['name'] ?></span></span></a>
                    <div class="dropdown-divider"></div><a onclick="logout()" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                  </div>
                </div>
              </li>
              <li class="dropdown dropdown-user nav-item">
                <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                  <span class="avatar avatar-online">

                  </span></a>
                <div class="dropdown-menu dropdown-menu-right">
                  <div class="arrow_box_right"><a class="dropdown-item" href="#"><span class="avatar avatar-online"><span class="user-name text-bold-700 ml-1"><?= $_SESSION['name'] ?></span></span></a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" onclick="app.logout()"><i class="ft-power"></i> Logout</a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    <!-- END: Head-->
  <?php } ?>