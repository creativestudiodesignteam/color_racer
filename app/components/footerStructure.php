<!-- <script src="public/js/jquery.js"></script> -->
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

<script src='public/js/jquery.smartmenus.min.js'></script>
<script src='public/js/jquery.prettyPhoto.js'></script>
<script src="public/js/jquery.sticky-kit.min.js"></script>
<script src='public/js/imagesloaded.pkgd.js'></script>
<script src='public/js/jquery.fitvids.js'></script>
<script src='public/js/tipper.js'></script>
<script src='public/js/swiper.min.js'></script>
<script src='public/js/main.js'></script>

<script src='public/js/countdowntime/countdowntime.js'></script>
<script src='public/js/countdowntime/moment.min.js'></script>
<script src='public/js/countdowntime/moment-timezone.min.js'></script>
<script src='public/js/countdowntime/moment-timezone-with-data.min.js'></script>

<script>
    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeYear: 0,
        endtimeMonth: 0,
        endtimeDate: 35,
        endtimeHours: 18,
        endtimeMinutes: 0,
        endtimeSeconds: 0,
        timeZone: "" 
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });
</script>
<!-- script FAQ -->
<script>
const toggles = document.querySelectorAll('.faq-toggle', '.faq-title' );

toggles.forEach(toggle => {
	toggle.addEventListener('click', () => {
		toggle.parentNode.classList.toggle('active');
	});
});
</script>

<!-- script video button youtube -->
<script>
var youtubeVideo = {
    videoBtn:'[data-videourl]',

    model: function() {

        function videoinit() {
            $('.container').on('click', youtubeVideo.videoBtn, function(event) {
            	event.preventDefault();
                var videoSrc = $(this).data('videourl');
              
                var ID = '';
                var url = videoSrc.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
                    if (url[2] !== undefined) {
                        ID = url[2].split(/[^0-9a-z_\-]/i);
                        ID = ID[0];
                    } else {
                        ID = url;
                    }

                var videoElement = $('<div class="video-popup-model">' + '<div class="video-layer">' + '<div class="video-model-close-layer">' + '</div>' + '<div class="model-wrapper">' + '<div class="videomodel">' + '<div class="videoscreen">' + '<iframe width="100%" height="auto" class="videlement"' + 'src="https://www.youtube.com/embed/' + ID + '?rel=0&amp;controls=1&amp;showinfo=0&amp;autoplay=1' + '" frameborder="0"' + 'allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"' + 'allowfullscreen></iframe>' + '</div>' + '<div class="modelCloseBtn">' + '</div>' + '</div>' + '</div>' + '</div>' + '</div>');

                    $('.container').prepend(videoElement);
                    var videoWidth = $('.video-popup-model .videlement').width();
                    var videHeight = (9 / 16) * videoWidth;
                    $('.video-popup-model .videlement').height(videHeight);
                    $('.container').find('.video-popup-model').addClass('smooth_show');
                });
        }
        videoinit();

        function modelClose() {
            $('.container').on('click', '.modelCloseBtn', function(event) {
                var model = $(this).parents('.video-popup-model')
                model.removeClass('smooth_show');
                setTimeout(function() {
                    model.remove();
                }, 500);
                $('.container').removeClass('no-reload');
            });
        }
        modelClose();

        function modelLayerClose() {
            $('.container').on('click', '.video-model-close-layer', function(event) {
                $(".modelCloseBtn").trigger('click');
            });
        }
        modelLayerClose();
    },
    init: function() {
        youtubeVideo.model();
    }
};

youtubeVideo.init();
</script>

</body>

</html>