<footer class="footer footer-static footer-light navbar-border navbar-shadow">
  <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">2020 &copy; Copyright <a class="text-bold-800 grey darken-2" href="https://www.creativedd.com.br" target="_blank">Creative Dev Design</a></span>
    <ul class="list-inline float-md-right d-block d-md-inline-blockd-none d-lg-block mb-0">
      <li class="list-inline-item"><a class="my-1" href="https://themeselection.com/" target="_blank"> More themes</a></li>

    </ul>
  </div>
</footer>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="../public/styles/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN Vendor JS-->
<script src="../public/styles/app-assets/js/scripts/extensions/dropzone.js" type="text/javascript"></script>
<!-- BEGIN: Page Vendor JS-->
<script src="../public/styles/app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>
<script src="../public/styles/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="../public/styles/app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="../public/styles/app-assets/js/core/app.js" type="text/javascript"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="../public/styles/app-assets/js/scripts/pages/dashboard-analytics.js" type="text/javascript"></script>

<!-- END: Page JS-->
<script src="../public/styles/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>

<!-- BEGIN: Page JS-->
<script src="../public/styles/app-assets/js/scripts/forms/form-login-register.js" type="text/javascript"></script>
<script src="../public/styles/app-assets/js/scripts/tables/datatables/datatable-basic.js" type="text/javascript"></script>
<script src="app-assets/js/scripts/pages/email-application.js" type="text/javascript"></script>


<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>