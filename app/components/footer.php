<!-- <script src="public/js/jquery.js"></script> -->
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>

<script src='public/js/jquery.smartmenus.min.js'></script>
<script src='public/js/jquery.prettyPhoto.js'></script>
<script src="public/js/jquery.sticky-kit.min.js"></script>
<script src='public/js/imagesloaded.pkgd.js'></script>
<script src='public/js/jquery.fitvids.js'></script>
<script src='public/js/tipper.js'></script>
<script src='public/js/swiper.min.js'></script>
<script src='public/js/main.js'></script>

<script src='public/js/countdowntime/countdowntime.js'></script>
<script src='public/js/countdowntime/moment.min.js'></script>
<script src='public/js/countdowntime/moment-timezone.min.js'></script>
<script src='public/js/countdowntime/moment-timezone-with-data.min.js'></script>

<script>
    $('.cd100').countdown100({
        /*Set Endtime here*/
        /*Endtime must be > current time*/
        endtimeYear: 0,
        endtimeMonth: 0,
        endtimeDate: 35,
        endtimeHours: 18,
        endtimeMinutes: 0,
        endtimeSeconds: 0,
        timeZone: "" 
        // ex:  timeZone: "America/New_York"
        //go to " http://momentjs.com/timezone/ " to get timezone
    });
</script>
</body>

</html>