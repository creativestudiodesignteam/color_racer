<?php
global $url, $db;
date_default_timezone_set('America/Sao_Paulo'); // Hora oficial do Brasil.
include 'app/model/events.model.php';
include 'app/controller/eventsDAO.php';
$events = new Events();
$eventsDAO = new EventsDAO($db);

$listAll = $eventsDAO->listAll();
setlocale(LC_ALL, NULL);
setlocale(LC_ALL, 'pt_BR');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <base href="http://<?= $_SERVER['SERVER_NAME'] ?>/color_racer/">
    <title><?= $url->title(); ?></title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700,900&display=swap" rel="stylesheet">


    <!-- Google Fonts -->
    <link rel="shortcut icon" href="public/images/favicon.ico" />
    <link rel="stylesheet" type="text/css" href='public/css/style.css' />

</head>


<body class="page-template-carousel">
    <div class="site-wrapper">
        <div class="doc-loader"></div>

        <!-- Left Part Sidebar -->
        <div class="menu-left-part">

            <nav id="header-main-menu">
                <ul class="main-menu sm sm-clean">
                    <li>
                        <a href="home">Home</a>
                    </li>
                    <li>
                        <a href="sobre">Sobre a Color</a>
                    </li>
                    <li>
                        <a class="has-submenu">Cidades</a>
                        <ul class="sub-menu">
                            <?php foreach ($listAll as $list) {
                            ?>
                                <li>
                                    <a href="cidades&id=<?= $list['id'] ?>"><?= $list['cities'] ?> - <?= date('d/m/Y', strtotime($list['date'])) ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li>
                        <a href="patrocinador">Patrocinador</a>
                    </li>
                    <li>
                        <a href="duvidas">Dúvidas</a>
                    </li>
                    <li>
                        <a href="contato">Contato</a>
                    </li>
                </ul>
                <!-- <form role="search" method="get" class="search-form" action="#">
                    <label>
                        <input autocomplete="off" type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:">
                    </label>
                </form> -->
            </nav>

            <div class="menu-image-kid">
                <img src="public/images/pretty/boy-menu-open.jpg" class="boy-menu" alt="Anotte">
            </div>
        </div>

        <!-- Right Part Sidebar -->
        <div class="menu-right-part">
            <div class="header-logo">
                <a href="home">
                    <img src="public/images/logo.png" alt="Anotte">
                </a>
            </div>

            <div class="toggle-holder">
                <div id="toggle">
                    <div class="menu-line"></div>
                </div>
            </div>

        </div>