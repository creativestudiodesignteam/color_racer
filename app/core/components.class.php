<?php
// Made by Guilherme Girão (https://github.com/guilhermegirao)

class Components extends Notification
{

    private $dir  = 'app/components/';

    public function setDir($dir)
    {
        $this->dir = $dir;
    }

    public function header()
    {
        require($this->dir . 'header.php');
    }
    public function headerADM()
    {
        require($this->dir . 'headerADM.php');
    }
    public function headerStructure()
    {
        require($this->dir . 'headerStructure.php');
    }

    public function modal($name)
    {
        require($this->dir . 'modal/' . $name . '.php');
    }

    public function footer()
    {
        require($this->dir . 'footer.php');
    }
    public function footerStructure()
    {
        require($this->dir . 'footerStructure.php');
    }
    public function footerADM()
    {
        require($this->dir . 'footerADM.php');
    }
}
