<?php
// Made by Guilherme Girão (https://github.com/guilhermegirao)

class URL extends Title
{

    public $link = '';
    private $dir  = 'app/view/';
    // private $admin = array('home', 'services/create' , 'services/edit', 'services/manage', 'consultancies/create', 'consultancies/edit', 'consultancies/manage', 'clinics/create', 'clinics/edit', 'clinics/manage', 'companies/create', 'companies/edit', 'companies/manage', 'medics/create', 'medics/edit', 'medics/manage', 'patients/create', 'patients/edit', 'patients/manage', 'users/create', 'users/edit', 'users/manage', 'scheduling/edit', 'scheduling/manage', 'financial/manage');
    // private $medic = array('home', 'medics/edit', 'medics/manage', );
    /* private $user = array('home'); */

    public function __construct()
    {
        $get = 'page';

        if (empty($_GET[$get])) $_GET[$get] = 'home';

        $this->link = $_GET[$get];

        if (!file_exists($this->dir . $this->link . '.view.php')) $this->link = 'error';
    }

    public function page()
    {
        global $db, $url, $components;
        if (explode("/", $url->link)[0] === "sistema") {
            if (empty($_SESSION['id'])) {
                $this->link = 'sistema/login';
            }
        }

        include($this->dir . $this->link . '.view.php');
    }

    public function redirect($page)
    {
        echo '<script>window.location = "./' . $page . '"</script>';
    }

    public function title()
    {
        return $this->pageTitle($this->link);
    }

    public function active($page)
    {
        if ($this->link == $page) {
            return 'active';
        } else {
            return '';
        }
    }

    public function filter($string)
    {
        $string = htmlspecialchars(addslashes(strip_tags($string)));
        return $string;
    }
}
