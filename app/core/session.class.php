<?php

class Session {

    public function start () {
        session_name ('__'.sha1(md5('secure'.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'])));
        session_start();
    }

    public function stop () {
        session_unset();
        session_destroy();
    }

}