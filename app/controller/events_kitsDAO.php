<?php

class Events_kitsDAO
{

    public $events_kits;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `events_kits` (fk_events, fk_kits, url, create_at, update_at) VALUES (:fk_events, :fk_kits, :url, :create_at, :update_at)");

        $insert->bindValue(":fk_events", $this->events_kits->getFk_events());
        $insert->bindValue(":fk_kits", $this->events_kits->getFk_kits());
        $insert->bindValue(":url", $this->events_kits->getUrl());
        $insert->bindValue(":create_at", $this->events_kits->getCreate_at());
        $insert->bindValue(":update_at", $this->events_kits->getUpdate_at());

        $insert->execute();
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `events_kits`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `events_kits` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function getByIdAll($id)
    {
        $list = $this->db->prepare("SELECT * FROM `events_kits` WHERE `fk_events`='$id'");
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `events_kits` WHERE `id` = :id");
        $delete->bindValue(":id", $this->events_kits->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `events_kits` SET fk_events = :fk_events, fk_kits = :fk_kits, url = :url WHERE id = :id");

        $update->bindValue(":id", $this->events_kits->getId());
        $update->bindValue(":fk_events", $this->events_kits->getFk_events());
        $update->bindValue(":fk_kits", $this->events_kits->getFk_kits());
        $update->bindValue(":url", $this->events_kits->getUrl());

        $update->execute();
    }
}
