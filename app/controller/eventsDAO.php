<?php

class EventsDAO
{

    public $events;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `events` (cities, place, date, url) VALUES (:cities, :place, :date, :url)");

        $insert->bindValue(":cities", $this->events->getCities());
        $insert->bindValue(":place", $this->events->getPlace());
        $insert->bindValue(":date", $this->events->getDate());
        $insert->bindValue(":url", $this->events->getUrl());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `events`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `events` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `events` WHERE `id` = :id");
        $delete->bindValue(":id", $this->events->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `events` SET cities = :cities, place = :place, date = :date, url = :url, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->events->getId());
        $update->bindValue(":cities", $this->events->getCities());
        $update->bindValue(":place", $this->events->getPlace());
        $update->bindValue(":date", $this->events->getDate());
        $update->bindValue(":url", $this->events->getUrl());
        $update->bindValue(":create_at", $this->events->getCreate_at());
        $update->bindValue(":update_at", $this->events->getUpdate_at());

        $update->execute();
    }
}
