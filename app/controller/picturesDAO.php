<?php

class PicturesDAO
{

    public $pictures;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `pictures` (name, path) VALUES (:name, :path)");

        $insert->bindValue(":name", $this->pictures->getName());
        $insert->bindValue(":path", $this->pictures->getPath());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `pictures`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `pictures` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `pictures` WHERE `id` = :id");
        $delete->bindValue(":id", $this->pictures->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `pictures` SET name = :name, path = :path WHERE id = :id");

        $update->bindValue(":id", $this->pictures->getId());
        $update->bindValue(":name", $this->pictures->getName());
        $update->bindValue(":path", $this->pictures->getPath());

        $update->execute();
    }
}
