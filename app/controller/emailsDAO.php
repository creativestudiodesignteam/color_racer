<?php

class EmailsDAO
{

    public $emails;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `emails` (name, email, subject, tellphone, city, mensage) VALUES (:name, :email, :subject, :tellphone, :city, :mensage)");

        $insert->bindValue(":name", $this->emails->getName());
        $insert->bindValue(":email", $this->emails->getEmail());
        $insert->bindValue(":subject", $this->emails->getSubject());
        $insert->bindValue(":tellphone", $this->emails->getTellphone());
        $insert->bindValue(":city", $this->emails->getCity());
        $insert->bindValue(":mensage", $this->emails->getMensage());

        $insert->execute();
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `emails`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `emails` WHERE `id` = :id");
        $delete->bindValue(":id", $this->emails->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `emails` SET name = :name, email = :email, subject = :subject, tellphone = :tellphone, city = :city, mensage = :mensage WHERE id = :id");

        $update->bindValue(":id", $this->emails->getId());
        $update->bindValue(":name", $this->emails->getName());
        $update->bindValue(":email", $this->emails->getEmail());
        $update->bindValue(":subject", $this->emails->getSubject());
        $update->bindValue(":tellphone", $this->emails->getTellphone());
        $update->bindValue(":city", $this->emails->getCity());
        $update->bindValue(":mensage", $this->emails->getMensage());

        $update->execute();
    }
}
