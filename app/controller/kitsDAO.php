<?php

class KitsDAO
{

    public $kits;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `kits` (name, value, description, fk_picture, create_at, update_at) VALUES (:name, :value, :description, :fk_picture, :create_at, :update_at)");

        $insert->bindValue(":name", $this->kits->getName());
        $insert->bindValue(":value", $this->kits->getValue());
        $insert->bindValue(":description", $this->kits->getDescription());
        $insert->bindValue(":fk_picture", $this->kits->getFk_picture());
        $insert->bindValue(":create_at", $this->kits->getCreate_at());
        $insert->bindValue(":update_at", $this->kits->getUpdate_at());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `kits` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `kits`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `kits` WHERE `id` = :id");
        $delete->bindValue(":id", $this->kits->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `kits` SET name = :name, value = :value, description = :description, fk_picture = :fk_picture, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->kits->getId());
        $update->bindValue(":name", $this->kits->getName());
        $update->bindValue(":value", $this->kits->getValue());
        $update->bindValue(":description", $this->kits->getDescription());
        $update->bindValue(":fk_picture", $this->kits->getFk_picture());
        $update->bindValue(":create_at", $this->kits->getCreate_at());
        $update->bindValue(":update_at", $this->kits->getUpdate_at());

        $update->execute();
    }
}
