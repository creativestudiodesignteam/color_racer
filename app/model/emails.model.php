<?php 

class Emails {

    private $id;
    private $name;
    private $email;
    private $subject;
    private $tellphone;
    private $city;
    private $mensage;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getSubject() {
        return $this->subject; 
    }

    public function getTellphone() {
        return $this->tellphone; 
    }

    public function getCity() {
        return $this->city; 
    }

    public function getMensage() {
        return $this->mensage; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setSubject($subject) {
        $this->subject = $subject; 
    }

    public function setTellphone($tellphone) {
        $this->tellphone = $tellphone; 
    }

    public function setCity($city) {
        $this->city = $city; 
    }

    public function setMensage($mensage) {
        $this->mensage = $mensage; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}