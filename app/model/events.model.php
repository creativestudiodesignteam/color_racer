<?php 

class Events {

    private $id;
    private $cities;
    private $place;
    private $date;
    private $url;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getCities() {
        return $this->cities; 
    }

    public function getPlace() {
        return $this->place; 
    }

    public function getDate() {
        return $this->date; 
    }

    public function getUrl() {
        return $this->url; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setCities($cities) {
        $this->cities = $cities; 
    }

    public function setPlace($place) {
        $this->place = $place; 
    }

    public function setDate($date) {
        $this->date = $date; 
    }

    public function setUrl($url) {
        $this->url = $url; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}