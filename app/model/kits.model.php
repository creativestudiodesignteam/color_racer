<?php 

class Kits {

    private $id;
    private $name;
    private $value;
    private $description;
    private $fk_picture;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getValue() {
        return $this->value; 
    }

    public function getDescription() {
        return $this->description; 
    }

    public function getFk_picture() {
        return $this->fk_picture; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setValue($value) {
        $this->value = $value; 
    }

    public function setDescription($description) {
        $this->description = $description; 
    }

    public function setFk_picture($fk_picture) {
        $this->fk_picture = $fk_picture; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}