<?php 

class Events_kits {

    private $id;
    private $fk_events;
    private $fk_kits;
    private $url;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getFk_events() {
        return $this->fk_events; 
    }

    public function getFk_kits() {
        return $this->fk_kits; 
    }

    public function getUrl() {
        return $this->url; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setFk_events($fk_events) {
        $this->fk_events = $fk_events; 
    }

    public function setFk_kits($fk_kits) {
        $this->fk_kits = $fk_kits; 
    }

    public function setUrl($url) {
        $this->url = $url; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}